<?php

    Class Model{

        private $server = "localhost";
        private $username = "root";
        private $password = "";
        private $db = "oop_crud";
        private $conn;

        public function __construct(){
            try {
                $this->conn = new mysqli($this->server,$this->username, $this->password,$this->db);
            } catch (Exception $e) {
                echo "connection failed" . $e->getMessage();
            }
        }

        public function insert(){
            if(isset($_POST['submit'])) {
                if (isset($_POST['sku']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['attibute']) && isset($_POST['details'])) {
                    if (!empty($_POST['sku']) && !empty($_POST['name']) && !empty($_POST['price']) && !empty($_POST['attribute']) && !empty($_POST['details'])) {
                        
                        $sku = $_POST['sku'];
                        $name = $_POST['name'];
                        $price = $_POST['price'];
                        $attribute = $_POST['attribute'];
                        $details = $_POST['details'];

                        $query = "INSERT INTO records (sku, name, price, attribute, details) VALUES ('$sku', '$name', '$price', '$attribute', '$details')";
                        if ($sql = $this->conn->query($query)) {
                            echo "<script>alert('records added successfully');</script>";
                            echo "<script>window.location.href = 'index.php';</script>";
                        }else {
                            echo "<script>alert('failed')</script>";
                            echo "<script>window.location.href = 'index.php';</script>";
                        }
                    }else {
                        echo "<script>alert('empty')</script>";
                        echo "<script>window.location.href = 'index.php';</script>";
                    }
                }
            }
        }

        public function fetch(){
            $data = null;

            $query = "SELECT * FROM 'records'";
            if ($sql = $this->conn->query($query)) {
                while ($row = mysqli_fetch_assoc($sql)) {
                    $data[] = $row;
                }
            }

            return $data;
        }

        public function delete($sku){
            $query = "DELETE FROM records where sku = '$sku";
            if ($sql = $this->conn->query($query)) {
                return true;
            }else{
                return false;
            }

        }

        public function fetch_single($sku){

            $data = null;

            $query = "SELECT * FROM records WHERE sku = '$sku'";
            if ($sql = $this->conn->query($query)) {
                while ($row = $sql->fetch_assoc()) {
                    $data = $row;
                }
            }
            return $data;
        }

        public function edit($sku){
            // echo $sku;
            $data = null;

            $query = "SELECT * FROM records WHERE sku = '$sku'";
            if ($sql = $this->conn->query($query)) {
                while ($row = $sql->fetch_assoc()) {
                    $data = $row;
                }
            }
            return $data;
        }

        public function update($data){

            $query = "UPDATE records SET name='$data[name]', price='$data[price]', attribute='$data[attribute]', details='$data[details]' WHERE sku='$data[sku]'";

            if ($sql = $this->conn->query($query)) {
                return true;
            }else {
                return false;
            }
        }

    }

?>