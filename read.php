<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <div class="container">

        <div class="row">
            <div class="col-md-12 mt-5">
                <h1 class="text-center">PHP OOP CRUD</h1>
                <hr style="height: 1px; color: black; background-color: black;">
            </div>
        </div>

        <div class="row">

            <div class="col-md-5 mx-auto">

                <?php 

                    include 'main.php';
                    $model = new Model();
                    $sku = $_REQUEST['sku'];
                    $row = $model->fetch_single($sku);

                    if(!empty($row)) {

                    

                ?>

                <div class="album py-5 bg-light">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                        xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                        focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                                        <title><?php echo $row['name']; ?></title>
                                        <rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%"
                                            fill="#eceeef" dy=".3em"><?php echo $row['attribute']; ?></text>
                                    </svg>
                                    <div class="card-body">
                                        <p class="card-text"><?php echo $row['sku']; ?></p>
                                        <p class="card-text"><?php echo $row['details']; ?></p>
                                        <p class="card-text"><?php echo $row['price']; ?></p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <a href="read.php?id=<?php echo $row['id']; ?>"
                                                    class="badge-info">Read</a>
                                                <a href="delete.php?id=<?php echo $row['id']; ?>"
                                                    class="badge-danger">Delete</a>
                                                <a href="edit.php?id=<?php echo $row['id']; ?>"
                                                    class="badge-success">Edit</a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php    
                    }else{
                        echo "Data not available.";
                    }
                ?>
            </div>

        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>

</html>