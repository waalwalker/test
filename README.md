# README #

Junior Developer Test (PHP)

### What is this repository for? ###

* A product addition and edit page
* One can add product
* One can also edit previously added product

### How do I get set up? ###

* XAMPP and VS code IDE is used
* PHP and HTML & CSS are used
* start Apache and MYSQL from XAMPP Control Panel

### Contribution guidelines ###

* Anyone can Contribute
* After editing, update in readme about edition

### Who do I talk to? ###

* Repo owner or admin
* Just drop a mail